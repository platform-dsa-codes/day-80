import java.util.Stack;

class MyQueue {
    private Stack<Integer> stack1;
    private Stack<Integer> stack2;

    public MyQueue() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }
    
    public void push(int x) {
        stack1.push(x); // Push to stack1 for enqueue operation
    }
    
    public int pop() {
        if (stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop()); // Move elements from stack1 to stack2 for dequeue operation
            }
        }
        return stack2.pop(); // Pop from stack2 for dequeue operation
    }
    
    public int peek() {
        if (stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop()); // Move elements from stack1 to stack2 for dequeue operation
            }
        }
        return stack2.peek(); // Peek from stack2 for dequeue operation
    }
    
    public boolean empty() {
        return stack1.isEmpty() && stack2.isEmpty(); // Queue is empty if both stacks are empty
    }
}

public class Main {
    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.push(1);
        myQueue.push(2);
        System.out.println(myQueue.peek()); // Output: 1
        System.out.println(myQueue.pop());  // Output: 1
        System.out.println(myQueue.empty()); // Output: false
    }
}
